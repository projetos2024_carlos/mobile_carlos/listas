import React from "react";
import { Button, View } from "react-native";
import { useNavigation } from "@react-navigation/native";

const HomeScreen = () => {
   const navigation = useNavigation(); // Adicione os parênteses aqui
   const handleUsers = () => {
      navigation.navigate("Users");
   }

   return (
      <View>
         <Button title="Ver usuários" onPress={handleUsers} />
      </View>
   )
};

export default HomeScreen
